output "instance_ip_addr" {
    value = aws_lightsail_static_ip.instance_ip.ip_address
}

output "username" {
    value = var.username_ubuntu
}

output "ssh_key" {
    value = "${var.key_pair_name}.pem"
}

output "ssh-connect" {
    value = "ssh -i ${var.key_pair_name}.pem ${var.username_ubuntu}@${aws_lightsail_static_ip.instance_ip.ip_address}"
}

output "mosh-connect-linux" {
    value = "mosh --ssh=\"ssh -i ${var.key_pair_name}.pem\" ${var.username_ubuntu}@${aws_lightsail_static_ip.instance_ip.ip_address}"
}

output "mosh-connect-blink-ios" {
    value = "mosh -I ${var.key_pair_name}.pem ${var.username_ubuntu}@${aws_lightsail_static_ip.instance_ip.ip_address}"
  }
