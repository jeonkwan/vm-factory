variable "aws_profile" {
    default = "default"
}

variable "region" {
    default = "ap-southeast-1"
    # Japan - ap-northeast-1
    # Singapore - ap-southeast-1
    # Korea - ap-northeast-2
}

variable "az" {
    # possible value: a,b,c
    default = "a"
}

variable "lightsail_instance_name" {
    default = "lightsail_machine"
}

variable "os" {
    default = "ubuntu_20_04"
}

variable "instance_type" {
    default = "nano_2_0"
}

variable "key_pair_name" {
    default = "lightsail_ssh_sg_jeonkwan"
    # lightsail_ssh_sg_jeonkwan
    # lightsail_ssh_jp_jeonkwan
    # lightsail_ssh_ko_jeonkwan
}

variable "username_ubuntu" {
    default = "ubuntu"
}

variable "docker_compose_version" {
    default = "1.29.2"
}
