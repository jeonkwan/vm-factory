provider "aws" {
  # version = "3.5.0"
  region  = var.region
  profile = var.aws_profile
}

terraform {
  # required_version = "~> 0.12.21"
}