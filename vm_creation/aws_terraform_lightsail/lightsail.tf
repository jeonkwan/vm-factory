resource "aws_lightsail_static_ip_attachment" "lightsail_instance_ip_attachment" {
  static_ip_name = aws_lightsail_static_ip.instance_ip.id
  instance_name  =  aws_lightsail_instance.lightsail_instance.id
}

resource "aws_lightsail_static_ip" "instance_ip" {
  name = "${var.lightsail_instance_name}_ip"
}

data "template_file" "vm_init_script" {
  template = "${file("${path.module}/setup_ubuntu.sh.tpl")}"
  vars = {
    username = var.username_ubuntu,
    docker_compose_version = var.docker_compose_version
  }
}

resource "aws_lightsail_instance" "lightsail_instance" {
  name              = var.lightsail_instance_name
  availability_zone = "${var.region}${var.az}"
  blueprint_id      = var.os
  bundle_id         = var.instance_type
  key_pair_name     = var.key_pair_name
  user_data         = data.template_file.vm_init_script.rendered
}

resource "aws_lightsail_instance_public_ports" "proxy" {
  instance_name = aws_lightsail_instance.lightsail_instance.name

  port_info {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
  }
  port_info {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
  }
  port_info {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
  }
  port_info {
    protocol    = "udp"
    from_port   = 60000
    to_port     = 60010
  }
  port_info {
    protocol    = "tcp"
    from_port   = 8990
    to_port     = 8990
  }
  port_info {
    protocol    = "udp"
    from_port   = 8990
    to_port     = 8990
  }
}

